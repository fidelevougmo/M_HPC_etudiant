
## training cpu

```
nix-shell --cores 12 -A cpu 
time python mnist_train.py 
exit
```

-> 1m1s


## training gpu 

```
nix-shell --cores 12 -A gpu 
time python mnist_train.py 
exit
```

-> 1m1s


## gui

```
python mnist_gui.py 
```

