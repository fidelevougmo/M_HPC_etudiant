{ pkgs, python }:

(pkgs.pybind11.override (pkgs: {
python = python;
})).overrideDerivation (attrs: {
  cmakeFlags = [ attrs.cmakeFlags "-DPYBIND11_TEST=OFF" ];
})

